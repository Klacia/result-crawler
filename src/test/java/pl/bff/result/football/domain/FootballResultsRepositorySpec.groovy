package pl.bff.result.football.domain

import groovy.time.TimeCategory

import java.time.ZonedDateTime
import java.time.temporal.ChronoField
import java.time.temporal.ChronoUnit;
import java.time.zone.ZoneOffsetTransitionRule.TimeDefinition;

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration

import pl.bff.result.Application
import pl.bff.result.domain.enums.ResultStatus
import spock.lang.Specification

@ContextConfiguration(classes = [Application], loader = SpringApplicationContextLoader)
@ActiveProfiles("test")
class FootballResultsRepositorySpec extends Specification {

	final String TEST_LEAGUE = "TST"
	ZonedDateTime today = ZonedDateTime.now()
	ZonedDateTime after16hours = today.truncatedTo(ChronoUnit.DAYS).plusHours(16)
	ZonedDateTime after20hours = today.truncatedTo(ChronoUnit.DAYS).plusHours(20)
	int maxResults = 20
	
	@Autowired
	private FootballResultRepository repo;
	
	def setup() {
		def results = []
		for(i in -20..maxResults){
			results << new FootballResult(date: today.plusDays(i), homeTeam: "Legia", visitorTeam: "Wisla", league:TEST_LEAGUE, score: new Score(4,0), status: ResultStatus.IN_PROGRESS, round: i) 
		}
		
		results << new FootballResult(date: after16hours, homeTeam: "Legia", visitorTeam: "Wisla", league:TEST_LEAGUE, score: new Score(4,0), status: ResultStatus.IN_PROGRESS, round: 10)
		results << new FootballResult(date: after20hours, homeTeam: "Legia", visitorTeam: "Wisla", league:TEST_LEAGUE, score: new Score(4,0), status: ResultStatus.IN_PROGRESS, round: 10)
		
		repo.save(results)
	}

	def cleanup() {
		repo.deleteAll()
	}

	def "should return past results"() {
		when:
			def result = repo.findAll(FootballSpecifications.pastByLeague(TEST_LEAGUE, 10))
		then:		
			result != null
			result.size() == 10
	}
	
	def "should return next fixtures"() {
		when:
			def result = repo.findAll(FootballSpecifications.nextByLeague(TEST_LEAGUE, 10))
		then:
			result != null
			result.size() == 10
	}
	
	def "should return live results"() {
		when:
			def result = repo.findAll(FootballSpecifications.liveByLeague(TEST_LEAGUE))
		then:
			result != null
			result.size() == 3
	}
	
}
