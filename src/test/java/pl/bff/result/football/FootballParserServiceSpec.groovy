package pl.bff.result.football;

import groovy.util.logging.Slf4j

import java.nio.charset.StandardCharsets

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.RestOperations

import pl.bff.result.Application
import pl.bff.result.domain.SiteRepository
import pl.bff.result.domain.enums.SiteType
import pl.bff.result.football.controller.FootballResultDTO;
import pl.bff.result.football.provider.FootballGroovyScriptRunner
import spock.lang.Specification

import com.google.common.io.CharStreams


@Slf4j
@ContextConfiguration(classes = [Application], loader = SpringApplicationContextLoader)
@ActiveProfiles("test")
public class FootballParserServiceSpec extends Specification {

	RestOperations restOper = Mock(RestOperations)
	
	@Autowired
	private SiteRepository siteRepo;
	
	@Autowired
	private FootballGroovyScriptRunner runner;
	
	private FootballParserService service;
	
	
	def "should return parsed results"(){
		given:
			service = new FootballParserService(siteRepo, runner, restOper);
			restOper.getForObject(_, String.class) >> getResourceAsString("sites/interia/interia_live.html")
		when:
			SiteParsingResult<FootballResultDTO> results = service.getResults("POL1", SiteType.FIXTURES)
			println results
		then:
			results != null
			results.getParsingResults()[0] instanceof FootballResultDTO
			results.getParsingResults().size() == 240
	}
	
	def "should return empty list if league code is not found"(){
		given:
			service = new FootballParserService(siteRepo, runner, restOper);
		when:
			SiteParsingResult<FootballResultDTO> results = service.getResults("WRONG_LEAGUE", SiteType.LIVE)
		then:
			results != null
			results.getParsingResults().size() == 0
	}
	
	def "should return empty list if page not change since last run"(){
		given:
			service = new FootballParserService(siteRepo, runner, restOper);
			restOper.getForObject(_, String.class) >> getResourceAsString("sites/interia/interia_live.html")
		when:
			SiteParsingResult<FootballResultDTO> resultsFirstRun = service.getResults("GER1", SiteType.FIXTURES)
			SiteParsingResult<FootballResultDTO> resultsSecRun = service.getResults("GER1", SiteType.FIXTURES)
		then:
			resultsFirstRun != null
			resultsFirstRun.getParsingResults().size() == 240
			resultsSecRun != null
			resultsSecRun.getParsingResults().size() == 0
	}
	
	private String getResourceAsString(String fileName) {
		try {
			final Reader reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName), StandardCharsets.UTF_8.name())
			return CharStreams.toString(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
