package pl.bff.result.football.provider;

import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.script.GroovyScriptRunner
import pl.bff.result.script.ScriptResult
import pl.bff.result.script.TestGroovyScripts
import spock.lang.Specification

public class FootballGroovyScriptRunnerSpec extends Specification {

	def "should evaluate groovy script"() {
		when:
			ScriptResult<List<FootballResultDTO>> results = new GroovyScriptRunner<List<FootballResultDTO>>().runScript(new TestGroovyScripts().footballScriptCode);
		then:		
			results.getReturnValue() != null
			results.getReturnValue()[0] instanceof FootballResultDTO
			results.getReturnValue().size() == 10
	}
	
}
