package pl.bff.result.football;

import java.time.ZonedDateTime
import java.time.ZoneId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration

import pl.bff.result.Application;
import pl.bff.result.domain.enums.ResultStatus
import pl.bff.result.football.FootballResultService
import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.football.domain.FootballResult
import pl.bff.result.football.domain.FootballResultRepository;
import pl.bff.result.football.domain.Score
import spock.lang.Specification

@ContextConfiguration(classes = [Application], loader = SpringApplicationContextLoader)
@ActiveProfiles("test")
public class FootballResultServiceSpec extends Specification {

	def parsedResults = [
		new FootballResultDTO(homeTeam: "Legia Warszawa", visitorTeam: "Wisła Kraków", status:ResultStatus.IN_PROGRESS , league: "PL1", score: new Score(2,1))
	]
	
	@Autowired
	private FootballResultService service;
	
	def "should detect that date is different"() {
		given:
		ZonedDateTime oldDate = ZonedDateTime.now()
		ZonedDateTime newDate = oldDate.plusDays(1)

		when:
		def result = service.isDateChanged(oldDate, newDate)

		then:
		result == true
	}
	
	def "should not detect that date is different when time is different and date is the same"() {
		given:
		ZonedDateTime oldDate = ZonedDateTime.now()
		ZonedDateTime newDate = oldDate.plusHours(1)

		when:
		def result = service.isDateChanged(oldDate, newDate)

		then:
		result == false
	}
		
	def "should detect changes is Result"(){
		expect:		
		FootballResult oldResult = new FootballResult(date: date1, homeTeam: "Legia Warszawa", visitorTeam: "Wisła Kraków", status:status1 , league: "PL1", score: score1)
		FootballResult newResult = new FootballResult(date: date2, homeTeam: "Legia Warszawa", visitorTeam: "Wisła Kraków", status:status2 , league: "PL1", score: score2)
		
		boolean serviceResult = service.isResultChanged(oldResult, newResult)
		
		serviceResult == result
		
		where:
		date1 					| status1 					| score1 			| date2   				 | status2 						| score2			|| result
		// no changes
		newDate(2015, 10, 10)  | ResultStatus.FINISHED 		| new Score(1, 0) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 0)	|| false
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 1)	|| false
		newDate(2015, 10, 10)  | ResultStatus.NOT_STARTED 	| new Score(1, 2) 	| newDate(2015, 10, 10) |	ResultStatus.NOT_STARTED	| new Score(1, 2)	|| false
		newDate(2015, 10, 10)  | ResultStatus.FINISHED 		| new Score(1, 0) 	| newDate(2015, 10, 10, 10, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 0)	|| false
		
		// detect status changes
		newDate(2015, 10, 10)  | ResultStatus.NOT_STARTED 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.FINISHED 		| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.NOT_STARTED	| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.FINISHED 		| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.NOT_STARTED 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.NOT_STARTED	| new Score(1, 1)	|| true
		
		// detect date changes
		newDate(2015, 11, 10)  | ResultStatus.FINISHED 		| new Score(1, 0) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 0)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 0) 	| newDate(2015, 11, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 0)	|| true
		newDate(2015, 10, 11)  | ResultStatus.FINISHED 		| new Score(1, 0) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 0)	|| true
		newDate(2015, 11, 11)  | ResultStatus.IN_PROGRESS 	| new Score(1, 0) 	| newDate(2015, 11, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 0)	|| true

		// detect score changes
		newDate(2015, 10, 10)  | ResultStatus.FINISHED 		| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.FINISHED		| new Score(1, 2)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(2, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 0) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 0) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(2, 0)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(0, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(1, 1)	|| true
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(0, 1) 	| newDate(2015, 10, 10) |	ResultStatus.IN_PROGRESS	| new Score(0, 2)	|| true
		
		// detect all changes
		newDate(2015, 10, 10)  | ResultStatus.IN_PROGRESS 	| new Score(1, 1) 	| newDate(2015, 10, 11) |	ResultStatus.FINISHED		| new Score(1, 2)	|| true
	}
	
	private ZonedDateTime newDate(int year, int month, int day, int hour = 10, int minute = 10, int sec = 10){
		ZonedDateTime dt = ZonedDateTime.of(year, month, day, hour, minute, sec, 0, ZoneId.of("Europe/Warsaw"))
		return dt
	}

}
