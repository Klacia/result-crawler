package pl.bff.result.script

class TestGroovyScripts {

	public final String scriptCodeReturnsString = """print 'KK'
return 'Legia'"""
	
	public final String scriptCodeReturnsPassedParam = """print 'KK2'
return "Legia "+param """
	
	public final String footballScriptCode = """import java.time.ZonedDateTime;

import groovy.transform.ToString;
import pl.bff.result.domain.enums.ResultStatus;
import pl.bff.result.football.domain.Score;
import pl.bff.result.football.controller.FootballResultDTO

	def retList = []

	for ( i in 0..9 ) {
    	retList << new FootballResultDTO(homeTeam: "Team home " + i, visitorTeam: "Visit team " + i)
	}
	return retList
"""
	
}
