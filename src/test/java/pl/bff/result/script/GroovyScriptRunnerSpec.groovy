package pl.bff.result.script;

import java.util.List;
import java.util.Map;

import org.codehaus.groovy.control.CompilationFailedException;

import groovy.lang.Binding;
import groovy.lang.GroovyShell
import pl.bff.result.football.controller.FootballResultDTO
import spock.lang.Specification;;;

public class GroovyScriptRunnerSpec extends Specification {
	
	def "should evaluate groovy script"() {
		given:
			ScriptRunner<String> scriptTest = new GroovyScriptRunner<String>();
		when:
			ScriptResult<String> results = scriptTest.runScript(new TestGroovyScripts().scriptCodeReturnsString);
		then:
			results != null
			results.getOutput().toString() == "KK"
			results.getReturnValue() == "Legia"
	}
	
	def "should evaluate groovy script with param"() {
		given:
			ScriptRunner<String> scriptTest = new GroovyScriptRunner<String>();
			Map<String, Object> params = ["param" : "King"]
		when:
			ScriptResult<String> results = scriptTest.runScript(new TestGroovyScripts().scriptCodeReturnsPassedParam, params);
		then:
			results != null
			results.getOutput().toString() == "KK2"
			results.getReturnValue() == "Legia King"
	}
	
	def "should throw exception from groovy script"() {
		when:
			ScriptRunner<List<FootballResultDTO>> results = new GroovyScriptRunner<String>().runScript("""def a = 3/0""");
		then:
			ScriptException ex = thrown()
			ex.message == "Unknown script error"
	}
	
	def "should throw exception when groovy script not compiling"() {
		when:
			ScriptRunner<List<FootballResultDTO>> results = new GroovyScriptRunner<Void>().runScript(""" = """);
		then:
			ScriptException ex = thrown()
			ex.message == "Script compilation failed"
	}
}
