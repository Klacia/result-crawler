import groovy.util.slurpersupport.GPathResult

import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Date;

import pl.bff.result.domain.enums.ResultStatus
import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.football.domain.Score
import org.ccil.cowan.tagsoup.Parser

XmlSlurper slurper = new XmlSlurper(new Parser())

String page = pageContent
GPathResult html = slurper.parse(new ByteArrayInputStream(page.bytes))
List<FootballResultDTO> resultList = []

//println "html = ${html}"
//http://wyniki.interia.pl/rozgrywki-R-polska-ekstraklasa,cid,3,sort,I

MonthHolder previousMonth = new MonthHolder()

html.'**'.findAll{ it.@class.text().startsWith("box boxSchedule")}?.each { box ->


	box.'**'.findAll{ it.@class.text().startsWith("box boxMatches")}?.each { boxMatches ->

		boxMatches.div.each {

			if (it.@class.text().startsWith("boxBody")){

				it.ul.li.each {

					FootballResultDTO match = new FootballResultDTO()
					match.homeTeam = it.'**'.find{it.@class == "team teamA"}.text()
					match.status = parseStatus(it.'**'.find{it.@class == "goals"}.text())
					match.score = parseScore(it.'**'.find{it.@class == "goals"}.text())
					match.visitorTeam = it.'**'.find{it.@class == "team teamB"}.text()
					match.round = parseRound(boxMatches.'**'.find{ it.@class?.text().startsWith("boxHeader")}?.text())

					def date = it.'**'.find{it.@class == "date"}.text()?.trim()
					
					if ("PRZEŁOŻONY".compareToIgnoreCase(date) == 0 ){
						match.date = new Date()
					} else { 
						if (date?.length() > 6) {
							match.date = toZonedDateTime(parseDateWithTime( date, previousMonth))
						} else {
							match.date = toZonedDateTime(parseDate( date, previousMonth))
						}
					}

					resultList.add(match)
					println "match = " + match.toString()
				}
			}
		}
	}

}

println "END"

return resultList

def extractInts( String input ) {
	return input?.findAll( /\d+/ )*.toInteger()
}

public Integer parseRound(String round){
	println "parseRound " + round
	def result = extractInts(round)
	if (result && result.size > 0){
		return result?.first()
	} else {
		return -1
	}
}

public Score parseScore(String score){
	println "parseScore " + score
	def scoreArray = score.split("-")
	if ( scoreArray.length == 2 && !score.contains("?") ) {
		return new Score(home: scoreArray[0].toInteger(), visitor: scoreArray[1].toInteger() )
	} else {
		return new Score(home: 0, visitor: 0)
	}
}


public Date parseDateWithTime(String date, MonthHolder previousMonth){
	println "parseDateWithTime " + date
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM HH:mm", Locale.US)

	Date dateVal
	try{
		dateVal = dateFormat.parse(date.trim())
		dateVal = setResultYear(dateVal, previousMonth)
	} catch (Exception ex){
		ex.printStackTrace()
		dateVal = null
	}
	return dateVal
}

public Date parseDate(String date, MonthHolder previousMonth){
	println "parseDate " + date

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.US)

	Date dateVal
	try{
		dateVal = dateFormat.parse(date.trim())
		dateVal = setResultYear(dateVal, previousMonth)
	} catch (Exception ex){
		ex.printStackTrace()
		dateVal = null
	}
	return dateVal
}

private Date setResultYear(Date parserMatchDate, MonthHolder previousMonth){
	Date now = new Date()
	println "previousMonth = " + previousMonth.month + " parserMatchDate = " + parserMatchDate
	if (previousMonth.month > parserMatchDate[Calendar.MONTH]){
		previousMonth.yearsToAdd++;
	}
	parserMatchDate[Calendar.YEAR] = now[Calendar.YEAR] + previousMonth.yearsToAdd
	previousMonth.month = parserMatchDate[Calendar.MONTH]
	
	return parserMatchDate
}

public ZonedDateTime toZonedDateTime(Date date){
	if(date != null)
		return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
	else
		return null
}

public ResultStatus parseStatus(String status) {
	println "parseStatus " + status
	ResultStatus result = ResultStatus.NOT_STARTED

	if ( status?.toLowerCase().trim() == "-" ) {
		result = ResultStatus.NOT_STARTED
	} else if ( status?.trim().contains("-") ) {
		result = ResultStatus.FINISHED
	} else if ( status?.trim().toLowerCase().contains("trwa") || status?.trim().toLowerCase().contains("przerwa")){
		result = ResultStatus.IN_PROGRESS
	}

	return result
}

class MonthHolder{
	int month = -1
	int yearsToAdd = 0
}

