import groovy.util.slurpersupport.GPathResult

import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime

import pl.bff.result.domain.enums.ResultStatus
import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.football.domain.Score
import org.ccil.cowan.tagsoup.Parser

XmlSlurper slurper = new XmlSlurper(new Parser())

String page = pageContent
GPathResult html = slurper.parse(new ByteArrayInputStream(page.bytes))
List<FootballResultDTO> resultList = []

//println "html = ${html}"
//http://wyniki.interia.pl/rozgrywki-R-polska-ekstraklasa,cid,3,sort,I

html.'**'.findAll{ it.@class.text().startsWith("box boxMatches")}?.first().each { box ->

	box.div.each {

		if (it.@class.text().startsWith("boxBody")){

			it.ul.li.each {

				FootballResultDTO match = new FootballResultDTO()
				match.homeTeam = it.'**'.find{it.@class == "team teamA"}.text()
				match.status = parseStatus(it.'**'.find{it.@class == "status"}.text())
				match.score = parseScore(it.'**'.find{it.@class == "goals"}.text())
				match.visitorTeam = it.'**'.find{it.@class == "team teamB"}.text()
				match.round = parseRound(box.'**'.find{ it.@class?.text().startsWith("title")}?.text())

				// extract date from details page
				def date
				String url = it.'**'.find{ it.@class.text().startsWith("score")}.a.@href
				//				println "url = [${url}]"
				/*if (url != null && url.trim().length() > 0) {
				 def subpage = slurper.parse("http://wyniki.interia.pl/${url}")
				 println "subpage = ${subpage}"
				 date = subpage.'**'.findAll{ it.@class.text() == "date"}?.first().text()
				 match.date = toZonedDateTime(parseDate2(date))
				 }*/

				if (ResultStatus.NOT_STARTED == match.status) {
					match.date = toZonedDateTime(parseDate2( it.'**'.find{it.@class == "date"}.text()))
				} else {
					match.date = toZonedDateTime(parseDate3( it.'**'.find{it.@class == "date"}.text()))
				}

				println "match = " + match.toString()
				
				if (ResultStatus.IN_PROGRESS == match.status) {
					resultList.add(match)
				} else {
					println "Mecz w niewłaściwym stanie"
				}
				
			}
		}
	}

}

println "END"

return resultList

def extractInts( String input ) {
	return input?.findAll( /\d+/ )*.toInteger()
}

public Integer parseRound(String round){
	println "parseRound " + round
	def result = extractInts(round)
	if (result && result.size > 0){
		return result?.first()
	} else {
		return -1
	}
}

public Score parseScore(String score){
	println "parseScore " + score
	def scoreArray = score.split("-")
	if ( scoreArray.length == 2 && !score.contains("?") ) {
		return new Score(home: scoreArray[0].toInteger(), visitor: scoreArray[1].toInteger() )
	} else {
		return new Score(home: 0, visitor: 0)
	}
}


public Date parseDate(String date){
	println "parseDate " + date
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US)

	Date dateVal
	try{
		dateVal =  dateFormat.parse(date.trim())
	} catch (Exception ex){
		dateVal = new Date()
	}
	return dateVal
}


public Date parseDate2(String date){
	println "parseDate2 " + date
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM HH:mm", Locale.US)

	Date dateVal
	Date now = new Date()
	try{
		dateVal =  dateFormat.parse(date.trim())
		dateVal[Calendar.YEAR] = now[Calendar.YEAR]
	} catch (Exception ex){
		ex.printStackTrace()
		dateVal = new Date()
	}
	//	println "parseDate2 returns " + dateVal
	return dateVal
}

public Date parseDate3(String date){
	println "parseDate3 " + date

	date = date.substring(0, date.indexOf(" "))

	println "parseDate3 substr" + date

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.US)

	Date dateVal
	Date now = new Date()
	try{
		dateVal =  dateFormat.parse(date.trim())
		dateVal[Calendar.YEAR] = now[Calendar.YEAR]
	} catch (Exception ex){
		ex.printStackTrace()
		dateVal = null
	}
	//	println "parseDate2 returns " + dateVal
	return dateVal
}

public ZonedDateTime toZonedDateTime(Date date){
	if(date != null)
		return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
	else
		return null
}

public ResultStatus parseStatus(String status) {
	println "parseStatus " + status
	ResultStatus result = ResultStatus.NOT_STARTED

	if ( status?.toLowerCase().contains("koniec") ) {
		result = ResultStatus.FINISHED
	} else if ( status?.trim().contains(":") ) {
		result = ResultStatus.NOT_STARTED
	} else if ( status?.trim().toLowerCase().contains("trwa") || status?.trim().toLowerCase().contains("przerwa")){
		result = ResultStatus.IN_PROGRESS
	}

	return result
}

