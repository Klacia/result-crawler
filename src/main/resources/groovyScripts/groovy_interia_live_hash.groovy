import org.ccil.cowan.tagsoup.Parser
import groovy.util.slurpersupport.GPathResult

XmlSlurper slurper = new XmlSlurper(new Parser())

String page = pageContent
GPathResult html = slurper.parse(new ByteArrayInputStream(page.bytes))
return html.'**'.find{ it.@class.text().startsWith("box boxMatches") }?.first().text()