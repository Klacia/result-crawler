package pl.bff.result.bootstrap;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.google.common.io.CharStreams;

import pl.bff.result.domain.Script;
import pl.bff.result.domain.ScriptRepository;
import pl.bff.result.domain.Site;
import pl.bff.result.domain.SiteRepository;
import pl.bff.result.domain.enums.SiteType;

@Component
public class DatabaseInitDataLoader implements CommandLineRunner {

	@Autowired
	private ScriptRepository scriptRepo;
	
	@Autowired
	private SiteRepository siteRepo;

	@Override
	public void run(String... args) throws Exception {
		
		System.out.println(" --------------------- DATABASE INIT --------------------- ");

		Map<String, String> leagueMap = new HashMap<>();
		leagueMap.put("POL1", "rozgrywki-R-polska-ekstraklasa,cid,3,sort,I");
		leagueMap.put("ENG1", "rozgrywki-L-anglia-premier-league,cid,619,sort,I");
		leagueMap.put("GER1", "rozgrywki-L-niemcy-bundesliga,cid,623,sort,I");
		leagueMap.put("SPA1", "rozgrywki-L-hiszpania-primera-division,cid,658,sort,I");
		leagueMap.put("ITA1", "rozgrywki-L-wlochy-serie-a,cid,659,sort,I");
		leagueMap.put("FRA1", "rozgrywki-L-francja-ligue-1,cid,621,sort,I");

		Script nextScr = new Script();
		nextScr.setName("Interia next results");
		nextScr.setContent(getResourceAsString("groovyScripts/groovy_interia_next_results.groovy"));
		scriptRepo.save(nextScr);
		
		Script nextHashScr = new Script();
		nextHashScr.setName("Interia next hash");
		nextHashScr.setContent(getResourceAsString("groovyScripts/groovy_interia_next_hash.groovy"));
		scriptRepo.save(nextHashScr);
		
		Script hashSrc = new Script();
		hashSrc.setName("Interia live hash");
		hashSrc.setContent(getResourceAsString("groovyScripts/groovy_interia_live_hash.groovy"));
		scriptRepo.save(hashSrc);
		
		Script parserSrc = new Script();
		parserSrc.setName("Interia live results");
		parserSrc.setContent(getResourceAsString("groovyScripts/groovy_interia_live_results.groovy"));
		scriptRepo.save(parserSrc);

		for (Entry<String, String> entry : leagueMap.entrySet()) {

			Site s1 = new Site();
			s1.setName("Interia.pl");
			s1.setUrl("http://wyniki.interia.pl/" + entry.getValue());
			s1.setType(SiteType.LIVE);
			s1.setLeagueCode(entry.getKey());
			s1.setContentChangeScrpt(hashSrc);
			s1.setContentParserScrpt(parserSrc);
			siteRepo.save(s1);

			Site s2 = new Site();
			s2.setName("Interia.pl");
			s2.setUrl("http://wyniki.interia.pl/" + entry.getValue());
			s2.setType(SiteType.FIXTURES);
			s2.setLeagueCode(entry.getKey());
			s2.setContentChangeScrpt(nextHashScr);
			s2.setContentParserScrpt(nextScr);
			siteRepo.save(s2);

		}
		
	}
	
	private String getResourceAsString(String fileName) {
		try (final Reader reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName), StandardCharsets.UTF_8.name())) {
			return CharStreams.toString(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";

	}
}
