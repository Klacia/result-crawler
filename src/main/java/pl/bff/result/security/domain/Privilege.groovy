package pl.bff.result.security.domain

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import pl.bff.result.domain.AuditBase;

@Entity
class Privilege extends AuditBase {

	private String name;
	
	@ManyToMany(mappedBy = "privileges")
	private Set<Role> roles;  
	
}
