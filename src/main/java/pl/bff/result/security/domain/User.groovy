package pl.bff.result.security.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import pl.bff.result.domain.AuditBase;

@Entity
public class User extends AuditBase{

	private String login;
	
	private String email;
	
	private String password;
	
	private boolean enabled;
	
	@ManyToMany
	private Set<Role> roles;

}
