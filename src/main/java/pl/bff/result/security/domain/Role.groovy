package pl.bff.result.security.domain

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import pl.bff.result.domain.AuditBase;

@Entity
class Role extends AuditBase{
	
	private String name;
	
	@ManyToMany(mappedBy="roles")
	private Set<User> users;
	
	@ManyToMany
	private Set<Privilege> privileges;
}
