package pl.bff.result.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

import com.google.common.base.MoreObjects;

@Entity
public class Script extends AuditBase {

	private static final long serialVersionUID = 3215051902938869009L;

	public String name;
	
	@NotNull
	@Column(nullable = false)
	@Lob
	public String content;
	
	@NotNull
	@Column(nullable = false)
	public boolean valid;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			       .add("id", getId())
			       .add("name", name)
			       .add("valid", valid)
			       .toString();
	}
}
