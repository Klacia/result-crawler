package pl.bff.result.domain;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import pl.bff.result.domain.enums.SiteType;

public interface SiteRepository extends PagingAndSortingRepository<Site, Long> {

	List<Site> findAllByLeagueCodeAndType(String leagueCode, SiteType type);

}
