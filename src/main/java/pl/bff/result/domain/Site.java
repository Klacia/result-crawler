package pl.bff.result.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import pl.bff.result.domain.enums.SiteType;

@Entity
public class Site extends AuditBase {

	private static final long serialVersionUID = 5320368644990946110L;

	@NotNull
	@Column(nullable = false)
	public String name;

	@NotNull
	@Column(nullable = false)
	public String url;

	@Column(nullable = true)
	@Lob
	public String contentHash;

	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	public SiteType type;

	@NotNull
	@Column(nullable = false)
	public String leagueCode;

	@OneToOne
	public Script contentChangeScript;

	@OneToOne
	public Script contentParserScript;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContentHash() {
		if (contentHash == null){
			contentHash = "";
		}
		return contentHash;
	}

	public void setContentHash(String contentHash) {
		this.contentHash = contentHash;
	}

	public SiteType getType() {
		return type;
	}

	public void setType(SiteType type) {
		this.type = type;
	}

	public String getLeagueCode() {
		return leagueCode;
	}

	public void setLeagueCode(String leagueCode) {
		this.leagueCode = leagueCode;
	}

	public Script getContentChangeScript() {
		return contentChangeScript;
	}

	public void setContentChangeScrpt(Script contentChangeScript) {
		this.contentChangeScript = contentChangeScript;
	}

	public Script getContentParserScript() {
		return contentParserScript;
	}

	public void setContentParserScrpt(Script contentParserScript) {
		this.contentParserScript = contentParserScript;
	}

}
