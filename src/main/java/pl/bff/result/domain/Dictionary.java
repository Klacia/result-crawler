package pl.bff.result.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dictionary implements Serializable {

	private static final long serialVersionUID = -8020466823868584282L;

	@Id
	@Column(name = "code")
    public String code;
   
	@Column(nullable = false, name = "eng_value")
    public String engValue;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEngValue() {
		return engValue;
	}

	public void setEngValue(String engValue) {
		this.engValue = engValue;
	}

}
