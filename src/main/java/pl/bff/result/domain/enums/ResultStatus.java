package pl.bff.result.domain.enums;

public enum ResultStatus {
	FINISHED, NOT_STARTED, IN_PROGRESS, POSTPONED
}