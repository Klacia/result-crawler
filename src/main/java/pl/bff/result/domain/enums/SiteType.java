package pl.bff.result.domain.enums;

public enum SiteType {

	PAST, FIXTURES, LIVE
	
}
