package pl.bff.result.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ScriptRepository extends PagingAndSortingRepository<Script, Long> {

}
