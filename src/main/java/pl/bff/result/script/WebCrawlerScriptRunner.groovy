package pl.bff.result.script;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

@Component
public class WebCrawlerScriptRunner {

	private ScriptRunner<String> grabber;

	private RestOperations restOperations;

	@Autowired
	public WebCrawlerScriptRunner(RestOperations restOperations) {
		super();
		this.grabber = new GroovyScriptRunner<String>();
		this.restOperations = restOperations;
	}

	public ScriptResult<String> runScript(String scriptCode, String url){
		String pageContent = restOperations.getForObject(url, String.class);
		return grabber.runScript(scriptCode, ["pageContent" : pageContent]);
	}
}
