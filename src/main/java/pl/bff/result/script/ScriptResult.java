package pl.bff.result.script;

import java.io.OutputStream;

public class ScriptResult<T> {

	T returnValue;
	OutputStream output;

	public ScriptResult(T returnValue, OutputStream output) {
		super();
		this.returnValue = returnValue;
		this.output = output;
	}

	public T getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(T result) {
		this.returnValue = result;
	}

	public OutputStream getOutput() {
		return output;
	}

	public void setOutput(OutputStream output) {
		this.output = output;
	}

}
