package pl.bff.result.script;

import java.util.Map;

public interface ScriptRunner<T> {

	ScriptResult<T> runScript(String scriptCode, Map<String, Object> scriptParameters);

	ScriptResult<T> runScript(String scriptCode);
	
}
