package pl.bff.result.script;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.groovy.control.CompilationFailedException;

import groovy.lang.GroovyShell;

public class GroovyScriptRunner<T> implements ScriptRunner<T> {
	
	private GroovyShell groovyShell = new GroovyShell(this.getClass().getClassLoader());
	
	@SuppressWarnings("unchecked")
	@Override
	public ScriptResult<T> runScript(String scriptCode, Map<String, Object> scriptParameters) {

		for (Entry<String, Object> entry : scriptParameters.entrySet()){
			groovyShell.setVariable(entry.getKey(), entry.getValue());
		}
		try {			
			OutputStream output = new ByteArrayOutputStream();
			groovyShell.setVariable("out", new PrintStream(output));
			T result = (T) groovyShell.evaluate(scriptCode);
			return new ScriptResult<T>(result, output);
		} catch (CompilationFailedException e) {
			throw new ScriptException("Script compilation failed", e);
		} catch (Throwable t){
			throw new ScriptException("Unknown script error", t);
		}
		
	}
	
	@Override
	public ScriptResult<T> runScript(String scriptCode) {
		return runScript(scriptCode, new HashMap<>());
	}
	
}
