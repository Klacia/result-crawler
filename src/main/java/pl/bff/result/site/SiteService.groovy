package pl.bff.result.site

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;

import pl.bff.result.domain.Site
import pl.bff.result.domain.enums.SiteType;
import pl.bff.result.football.FootballParserService
import pl.bff.result.football.FootballResultService
import pl.bff.result.football.SiteParsingResult
import pl.bff.result.football.controller.FootballResultDTO

@Service
class SiteService {
	
	private FootballResultService resultService;
	private FootballParserService parserService;

	@Autowired
	public SiteService(FootballResultService resultService, FootballParserService parserService) {
		this.resultService = resultService;
		this.parserService = parserService;
	}
	
	public SiteParsingResult<FootballResultDTO> grabResults(String leagueCode, SiteType siteType){
		SiteParsingResult<FootballResultDTO> resultsPast = parserService.getResults(leagueCode, siteType);
		return resultService.saveParsedResults(resultsPast);
	}
	
	public SiteParsingResult<FootballResultDTO> grabResults(Site site){
		SiteParsingResult<FootballResultDTO> resultsPast = parserService.getResults(site.leagueCode, site.type);
		return resultService.saveParsedResults(resultsPast);
	}
	
}
