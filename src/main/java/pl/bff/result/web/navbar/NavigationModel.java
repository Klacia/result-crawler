package pl.bff.result.web.navbar;

public class NavigationModel {

	private MenuOption active;

	public NavigationModel(MenuOption active) {
		this.active = active;
	}

	public String getActive() {
		return active.getName();
	}

}
