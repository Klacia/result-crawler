package pl.bff.result.web.navbar;

public enum MenuOption {

	INDEX(""), CONFIG("config"), RESUTLS("results");
	
	private String name;

	private MenuOption(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
