package pl.bff.result.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.bff.result.web.navbar.MenuOption;
import pl.bff.result.web.navbar.NavigationModel;

@Controller
@RequestMapping("/")
public class IndexController {

	
	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("navResource", new NavigationModel(MenuOption.INDEX));
        return "index";
	}
	
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login() {
		System.out.println("login page served");
        return "login";
	}
}
