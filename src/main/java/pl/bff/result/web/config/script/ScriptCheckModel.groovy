package pl.bff.result.web.config.script;

import groovy.transform.ToString
import pl.bff.result.domain.Site
import pl.bff.result.domain.Script

@ToString
public class ScriptCheckModel {

	private Long id
	private String name
	private String content
	private String url
	private String scriptOutput
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getScriptOutput() {
		return scriptOutput;
	}
	public void setScriptOutput(String scriptOutput) {
		this.scriptOutput = scriptOutput;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}