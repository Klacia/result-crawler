package pl.bff.result.web.config;

public class LeftMenuModel {

	private LeftMenuOption active;

	public LeftMenuModel(LeftMenuOption active) {
		this.active = active;
	}

	public String getActive() {
		return active.getName();
	}

}
