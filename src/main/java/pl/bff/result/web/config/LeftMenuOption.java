package pl.bff.result.web.config;

public enum LeftMenuOption {

	SITE("site"), SCRIPT("script");
	
	private String name;

	private LeftMenuOption(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
