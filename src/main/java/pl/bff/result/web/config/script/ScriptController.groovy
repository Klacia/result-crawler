package pl.bff.result.web.config.script;

import groovy.util.logging.Slf4j
import javassist.bytecode.stackmap.BasicBlock.Catch;

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.bff.result.domain.Script
import pl.bff.result.domain.ScriptRepository
import pl.bff.result.domain.Site
import pl.bff.result.domain.SiteRepository
import pl.bff.result.script.ScriptException;
import pl.bff.result.script.ScriptResult;
import pl.bff.result.script.WebCrawlerScriptRunner
import pl.bff.result.web.BaseController
import pl.bff.result.web.alerts.AlertModel
import pl.bff.result.web.alerts.AlertOption
import pl.bff.result.web.config.LeftMenuModel
import pl.bff.result.web.config.LeftMenuOption
import pl.bff.result.web.navbar.MenuOption
import pl.bff.result.web.navbar.NavigationModel

@Controller
@RequestMapping("/config/script")
@Slf4j
public class ScriptController extends BaseController {

	@Autowired
	private ScriptRepository scriptRepo;

	@Autowired
	private SiteRepository siteRepo;

	@Override
	public LeftMenuModel getLeftMenuModel() {
		return new LeftMenuModel(LeftMenuOption.SCRIPT);
	}

	@Override
	public NavigationModel getTopMenuModel() {
		return new NavigationModel(MenuOption.CONFIG);;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("scripts", scriptRepo.findAll());
		return "script/script-list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addScript(@RequestParam("siteId") Long siteId, @RequestParam("scriptType") ScriptType scriptType, final Model model) {
		ScriptModel scriptModel = new ScriptModel(new Script(), scriptType, siteId)
		model.addAttribute("script", scriptModel);
		return "script/script-edit";
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String editScript(@PathVariable("id") Long id,
			final @RequestParam(value = "scriptType", required=false) ScriptType scriptType,
			@RequestParam(value = "siteId", required=false) Long siteId,
			final Model model) {
		Script script = scriptRepo.findOne(id);
		ScriptModel scriptModel = new ScriptModel(script, scriptType, siteId)
		model.addAttribute("script", scriptModel);
		return "script/script-edit";
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public String deleteScript(@PathVariable("id") Long id, final Model model) {
		Script script = scriptRepo.delete(id);
		return "script/script-list";
	}

	/**
	 * @param updatedScript
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveScript(@ModelAttribute ScriptModel updatedScript, final Model model) {

		Script script = null;
		if (updatedScript.id != null){
			script = scriptRepo.findOne(updatedScript.id);
			script.setName(updatedScript.name);
			script.setContent(updatedScript.content);
			script.setValid(updatedScript.valid);
			script = scriptRepo.save(script);
		} else {
			script = new Script();
			script.setName(updatedScript.name);
			script.setContent(updatedScript.content);
			script.setValid(updatedScript.valid);
			script = scriptRepo.save(script);

			Site site = siteRepo.findOne(updatedScript.siteId);
			switch(updatedScript.scriptType){
				case ScriptType.HASH:
					site.setContentChangeScrpt(script);
					break;
				case ScriptType.PARSER:
					site.setContentParserScrpt(script);
					break;
				default:
					break;
			}
			site = siteRepo.save(site);
		}

		model.addAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Script updated"));
		return editScript(script.getId(), updatedScript.scriptType, updatedScript.siteId, model);
	}

	@RequestMapping(value = "/{id}/check", method = RequestMethod.GET)
	public String checkScript(@PathVariable("id") Long id, final Model model) {
		Script script = scriptRepo.findOne(id);
		ScriptCheckModel scriptCheckModel = new ScriptCheckModel(id: script.id, content:script.getContent(), name: script.getName())
		model.addAttribute("script", scriptCheckModel);
		return "script/script-check";
	}

	@RequestMapping(value = "/{id}/check", method = RequestMethod.POST)
	public String checkScript(@PathVariable("id") Long id, @ModelAttribute ScriptCheckModel updatedScript, final Model model) {
		log.info("Checking script {}", updatedScript.id)
		Script script = scriptRepo.findOne(id);
		
		script.setContent(updatedScript.getContent())
		scriptRepo.save(script)
		
//		try{
//			ScriptResult<String> scriptResult = webScriptRunner.runScript(updatedScript.content, updatedScript.url)
//			updatedScript.setScriptOutput(scriptResult.output.toString())
//			model.addAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Script checked"));
//		} catch (Exception e){
//			log.error("Error executing script {} for url {}", updatedScript.content, updatedScript.url, e)
//			model.addAttribute("alert", new AlertModel(AlertOption.DANGER, "Error executing script: " + e.getMessage()))
//		}

		model.addAttribute("script", updatedScript);
		return "script/script-check";
	}
	
	
}
