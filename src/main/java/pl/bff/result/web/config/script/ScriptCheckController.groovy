package pl.bff.result.web.config.script;

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

import pl.bff.result.script.ScriptResult
import pl.bff.result.script.WebCrawlerScriptRunner

@RestController
@RequestMapping("/config/script")
@Slf4j
public class ScriptCheckController {

	@Autowired
	private WebCrawlerScriptRunner webScriptRunner;

	@RequestMapping(value = "/{id}/checkAjax", method = RequestMethod.POST)
	public String checkScriptAjax(@PathVariable("id") Long id, @RequestBody ScriptCheckJson scriptCheckJson) {
		log.info("Checking ajax script {} | \n url {}", id, scriptCheckJson.url)

		String result = "";
		try{
			ScriptResult<String> scriptResult = webScriptRunner.runScript(scriptCheckJson.content, scriptCheckJson.url)
			result = scriptResult.output.toString()
		} catch (Exception e){
			log.error("Error executing script {} for url {}", scriptCheckJson.content, scriptCheckJson.url, e)
			result = "Error executing script: " + e.getMessage()
		}
		log.info("result = {}", result);

		return result;
	}
}
