package pl.bff.result.web.config.script;

import groovy.transform.ToString
import pl.bff.result.domain.Site
import pl.bff.result.domain.Script

@ToString
public class ScriptModel {

	private Long id
	private String name
	private boolean valid
	private String content
	private ScriptType scriptType
	private Long siteId
	
	public ScriptModel(){}
	
	public ScriptModel(Script script) {
		super();
		this.id = script.getId();
		this.name = script.getName();
		this.valid = script.isValid();
		this.content = script.getContent();
	}

	public ScriptModel(Script script, ScriptType scriptType, Long siteId) {
		this(script, scriptType);
		this.siteId = siteId;
	}
	
	public ScriptModel(Script script, ScriptType scriptType) {
		this(script);
		this.scriptType = scriptType;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public ScriptType getScriptType() {
		return scriptType;
	}
	public void setScriptType(ScriptType scriptType) {
		this.scriptType = scriptType;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	
}