package pl.bff.result.web.config;

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import pl.bff.result.domain.Site
import pl.bff.result.domain.SiteRepository
import pl.bff.result.football.SiteParsingResult;
import pl.bff.result.football.controller.FootballResultDTO;
import pl.bff.result.site.SiteService
import pl.bff.result.web.BaseController
import pl.bff.result.web.alerts.AlertModel
import pl.bff.result.web.alerts.AlertOption
import pl.bff.result.web.navbar.MenuOption
import pl.bff.result.web.navbar.NavigationModel

@Controller
@RequestMapping("/config")
@Slf4j
public class ConfigController extends BaseController{

	@Autowired
	private SiteRepository siteRepo;
	
	@Autowired
	private SiteService siteService;
	
	@Override
	public LeftMenuModel getLeftMenuModel() {
		return new LeftMenuModel(LeftMenuOption.SITE);
	}
	
	@Override
	public NavigationModel getTopMenuModel() {
		return new NavigationModel(MenuOption.CONFIG);;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("navResource", new NavigationModel(MenuOption.CONFIG));
		model.addAttribute("sites", siteRepo.findAll());
		return "site/site-list";
	}
	
	@RequestMapping(value = "/site", method = RequestMethod.GET)
	public String listSites(Model model) {
		return list(model);
	}

	@RequestMapping(value = "/site/delete/{id}", method = RequestMethod.GET)
	public String deleteSite(@PathVariable("id") Long id, final RedirectAttributes redirectAttributes) {
		siteRepo.delete(id);
		redirectAttributes.addFlashAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Site deleted"));
		return "redirect:/config";
	}
	
	@RequestMapping(value = "/site/parse/{id}", method = RequestMethod.GET)
	public String parseSite(@PathVariable("id") Long id, final RedirectAttributes redirectAttributes) {
		Site site = siteRepo.findOne(id);
		SiteParsingResult<FootballResultDTO> results = siteService.grabResults(site);
		if (results.pageNotChanged){
			redirectAttributes.addFlashAttribute("alert", new AlertModel(AlertOption.INFO, "Site didn't changed. Parsing not needed"));
		} else {
			redirectAttributes.addFlashAttribute("alert", new AlertModel(AlertOption.INFO, "Parsing results (new: $results.newResults, changed: $results.changedResults)"));
		}
		return "redirect:/config";
	}

	@RequestMapping(value = "/site/{id}", method = RequestMethod.GET)
	public String viewSite(@PathVariable("id") Long id, final Model model) {
		model.addAttribute("navResource", new NavigationModel(MenuOption.CONFIG));

		Site site = siteRepo.findOne(id);
		model.addAttribute("site", site);

		return "site/site-view";
	}
	
	@RequestMapping(value = "/site/add", method = RequestMethod.GET)
	public String addSite(final Model model) {
		model.addAttribute("navResource", new NavigationModel(MenuOption.CONFIG));

		Site site = new Site();		
		model.addAttribute("site", site);

		return "site/site-edit";
	}

	@RequestMapping(value = "/site/edit/{id}", method = RequestMethod.GET)
	public String editSite(@PathVariable("id") Long id, final Model model) {
		model.addAttribute("navResource", new NavigationModel(MenuOption.CONFIG));

		Site site = siteRepo.findOne(id);
		model.addAttribute("site", site);

		return "site/site-edit";
	}
	

	@RequestMapping(value = "/site/edit", method = RequestMethod.POST)
	public String saveSite(@ModelAttribute Site updatedSite, final Model model) {
		
		Site site = null;
		
		if(updatedSite.getId() != null) {
			site = siteRepo.findOne(updatedSite.getId());
			site.setName(updatedSite.getName());
			site.setUrl(updatedSite.getUrl());
			site.setType(updatedSite.getType());
			site.setLeagueCode(updatedSite.getLeagueCode());
		} else {
			site = updatedSite;
		}
		
		siteRepo.save(site);
		
		model.addAttribute("navResource", new NavigationModel(MenuOption.CONFIG));
		model.addAttribute("site", site);
		
		model.addAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Site updated"));
		
		return "site/site-edit";
	}

}
