package pl.bff.result.web.alerts;

public enum AlertOption {

	SUCCESS("success"), INFO("info"), WARNING("warning"), DANGER("danger");
	
	private String name;

	private AlertOption(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
