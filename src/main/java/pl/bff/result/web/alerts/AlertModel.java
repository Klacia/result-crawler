package pl.bff.result.web.alerts;

public class AlertModel {

	private AlertOption type;
	private String message;

	public AlertModel(AlertOption type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type.getName();
	}

	public String getMessage() {
		return message;
	}

}
