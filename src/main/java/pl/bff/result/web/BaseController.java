package pl.bff.result.web;

import org.springframework.web.bind.annotation.ModelAttribute;

import pl.bff.result.web.config.LeftMenuModel;
import pl.bff.result.web.navbar.NavigationModel;

public abstract class BaseController {

	@ModelAttribute("navResource")
	public abstract NavigationModel getTopMenuModel();

	@ModelAttribute("configLeftMenu")
	public abstract LeftMenuModel getLeftMenuModel();

}
