package pl.bff.result.web.results;

import groovy.util.logging.Slf4j

import java.time.ZoneId;
import java.time.ZonedDateTime

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.web.SortDefault
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import pl.bff.result.football.domain.FootballResult
import pl.bff.result.football.domain.FootballResultRepository
import pl.bff.result.web.BaseController
import pl.bff.result.web.alerts.AlertModel
import pl.bff.result.web.alerts.AlertOption
import pl.bff.result.web.config.LeftMenuModel
import pl.bff.result.web.navbar.MenuOption
import pl.bff.result.web.navbar.NavigationModel

@Controller
@RequestMapping("/results")
@Slf4j
public class ResultsController extends BaseController {

	@Autowired
	private FootballResultRepository resultsRepo;

	@RequestMapping(method = RequestMethod.GET)
	public String results(Model model, @SortDefault("date") Pageable pageable) {
		Page<FootballResult> page = resultsRepo.findAll(pageable);

		model.addAttribute("results",  new PageImpl<FootballResultModel>(page.getContent().collect{FootballResultModel.fromFootballResult(it)}, pageable, page.getTotalElements()));
		return "result/list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String edit(final Model model) {
		model.addAttribute("result", new FootballResultModel());
		return "result/edit";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Model model, @ModelAttribute("result") @Valid FootballResultModel newResultModel, Errors errors, RedirectAttributes redirectAttributes) {

		if(errors.hasErrors()){
			log.info("form has errors {}" , errors.getAllErrors())
			model.addAttribute("result", newResultModel)
			return "result/edit"
		}

		log.info("Saving result {} {} {}" , newResultModel.date, newResultModel.league, newResultModel.score)
		FootballResult result = null;
		if (newResultModel.id){
			result = resultsRepo.findOne(newResultModel.id);

			log.info( "locale {} ", LocaleContextHolder.getTimeZone());

			result.setDate(ZonedDateTime.ofInstant(newResultModel.date.toInstant(), LocaleContextHolder.getTimeZone().toZoneId() ));
			result.setScore(newResultModel.score);
			result.setHomeTeam(newResultModel.homeTeam);
			result.visitorTeam = newResultModel.visitorTeam;
			result.status = newResultModel.status;
			result.league = newResultModel.league;
			result.round = newResultModel.round
		} else {
			result = newResultModel.toFootballResult()
		}
		result = resultsRepo.save(result);
		
		redirectAttributes.addFlashAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Result saved"));
		return "redirect:/results/${result.getId()}/edit";
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, final Model model) {
		FootballResult result = resultsRepo.findOne(id);
		model.addAttribute("result", FootballResultModel.fromFootballResult(result));
		return "result/edit";
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		resultsRepo.delete(id);
		redirectAttributes.addFlashAttribute("alert", new AlertModel(AlertOption.SUCCESS, "Result deleted"));
		return "redirect:/results";
	}

	@Override
	public NavigationModel getTopMenuModel() {
		return new NavigationModel(MenuOption.RESUTLS);
	}

	@Override
	public LeftMenuModel getLeftMenuModel() {
		return null;
	}

}
