package pl.bff.result.web.results;

import java.time.ZonedDateTime;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;

import pl.bff.result.domain.enums.ResultStatus;
import pl.bff.result.football.domain.FootballResult;
import pl.bff.result.football.domain.Score;

public class FootballResultModel {

	public Long id;

	@NotNull
	public Score score;

	@NotNull
	@Size(min = 2)
	public String homeTeam;

	@NotNull
	@Size(min = 2)
	public String visitorTeam;

	@NotNull
	public ResultStatus status;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date date;

	@NotNull
	@Size(min = 3)
	public String league;

	@NotNull
	@Min(1)
	public Integer round;

	public static FootballResultModel fromFootballResult(FootballResult fr) {
		FootballResultModel model = new FootballResultModel();
		model.id = fr.getId();
		model.score = fr.score;
		model.homeTeam = fr.homeTeam;
		model.visitorTeam = fr.visitorTeam;
		model.status = fr.status;
		model.league = fr.league;
		model.round = Integer.valueOf(fr.round);

		if (fr.getDate() != null) {
			model.date = Date.from(fr.getDate().toInstant());
		}

		return model;
	}

	public FootballResult toFootballResult() {
		FootballResult result = new FootballResult();
		result.setId(this.getId());
		result.score = this.score;
		result.homeTeam = this.homeTeam;
		result.visitorTeam = this.visitorTeam;
		result.status = this.status;
		result.league = this.league;
		result.round = this.round;

		if (this.date != null) {
			result.setDate(ZonedDateTime.ofInstant(this.date.toInstant(), LocaleContextHolder.getTimeZone().toZoneId()));
		}

		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Score getScore() {
		return score;
	}

	public void setScore(Score score) {
		this.score = score;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getVisitorTeam() {
		return visitorTeam;
	}

	public void setVisitorTeam(String visitorTeam) {
		this.visitorTeam = visitorTeam;
	}

	public ResultStatus getStatus() {
		return status;
	}

	public void setStatus(ResultStatus status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Integer getRound() {
		return round;
	}

	public void setRound(Integer round) {
		this.round = round;
	}

}
