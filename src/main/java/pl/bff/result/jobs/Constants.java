package pl.bff.result.jobs;

public class Constants {

	public static final String JOB_CRON_EVERY_1_HOUR = "0 0 * * * *";
	
	public static final String JOB_CRON_EVERY_5_MINUTES = "0 */5 * * * *";
	
}
