package pl.bff.result.jobs;

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

import pl.bff.result.domain.Site
import pl.bff.result.domain.SiteRepository
import pl.bff.result.football.SiteParsingResult
import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.site.SiteService

@Component
@Slf4j
public class ScheduledTasks {


	@Autowired
	private SiteService siteService;

	@Autowired
	private SiteRepository siteRepo;

	@Scheduled(cron = Constants.JOB_CRON_EVERY_1_HOUR)
	public void runSiteParsing() {
		log.info("JOB site parsing started")

		siteRepo.findAll().each { Site s ->
			try {
				SiteParsingResult<FootballResultDTO> results = siteService.grabResults(s);
				if(results.pageNotChanged){
					log.info("Page {} [{}] not changed", s.name, s.url)
				} else {
					log.info("Parsing site {} [{}] returns new: {} and changed: {}", s.name, s.url, results.newResults, results.changedResults)
				}
			} catch (Exception e) {
				log.error("Error during parsing site {} [{}]", s.name, s.url , e);
			}
		}

		log.info("JOB site parsing ended")
	}
}