package pl.bff.result.football;

import groovy.util.logging.Slf4j

import java.nio.charset.StandardCharsets

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.client.RestOperations

import pl.bff.result.domain.Site
import pl.bff.result.domain.SiteRepository
import pl.bff.result.domain.enums.SiteType
import pl.bff.result.football.controller.FootballResultDTO
import pl.bff.result.football.provider.FootballGroovyScriptRunner

import com.google.common.hash.HashCode
import com.google.common.hash.HashFunction
import com.google.common.hash.Hasher
import com.google.common.hash.Hashing

@Component
@Slf4j
public class FootballParserService {

	private SiteRepository siteRepo;
	private FootballGroovyScriptRunner runner;
	private RestOperations restOperations;

	@Autowired
	public FootballParserService(SiteRepository siteRepo, FootballGroovyScriptRunner runner,
	RestOperations restOperations) {
		super();
		this.siteRepo = siteRepo;
		this.runner = runner;
		this.restOperations = restOperations;
	}

	public SiteParsingResult<FootballResultDTO> getResults(String leagueCode, SiteType siteType) {
		log.debug("get results for {} {}", leagueCode, siteType)
		
		SiteParsingResult<FootballResultDTO> retVal = new SiteParsingResult<FootballResultDTO>()
		
		for (Site site : siteRepo.findAllByLeagueCodeAndType(leagueCode, siteType)) {

			String pageContent = restOperations.getForObject(site.getUrl(), String.class);
			String pageHash = hashString(runner.getSiteHash(site.getContentChangeScript(), pageContent).getReturnValue());

			if (isSiteHashChanged(site, pageHash)) {
				retVal.addAllResult(runner.getResults(site.getContentParserScript(), pageContent).getReturnValue());
				site.setContentHash(pageHash);
				siteRepo.save(site)
			} else {
				retVal.pageNotChanged = true;
				log.debug("Page {} not changed", site.getUrl())
			}
		}
		
		//FIXME zle to wyglada :/ 
		retVal.getParsingResults().each{ FootballResultDTO f ->
			f.league = leagueCode
		}
		log.debug("Parser {} results", retVal.getParsingResults().size())
		
		return retVal;
	}
	
	private boolean isSiteHashChanged(Site site, String currentHash){
		return site.getContentHash().compareToIgnoreCase(currentHash) != 0
	}

	private String hashString(String content){
		log.debug("Hashing string {}", content)
		HashFunction hf = Hashing.sha256();
		Hasher hasher = hf.newHasher();
		hasher.putString(content, StandardCharsets.UTF_8);
		HashCode hash = hasher.hash();
		return hash.toString();
	}
}
