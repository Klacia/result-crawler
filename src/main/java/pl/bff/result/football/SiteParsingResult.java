package pl.bff.result.football;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SiteParsingResult<T> {

	public List<T> parsingResults = new LinkedList<T>();;
	public long newResults;
	public long changedResults;
	public boolean pageNotChanged = false;
	
	public boolean addResult(T result){
		return getParsingResults().add(result);
	}
	
	public boolean addAllResult(Collection<T> result){
		return getParsingResults().addAll(result);
	}

	public List<T> getParsingResults() {
		return parsingResults;
	}

	public void setParsingResults(List<T> parsingResults) {
		this.parsingResults = parsingResults;
	}

	public long getNewResults() {
		return newResults;
	}

	public void setNewResults(long newResults) {
		this.newResults = newResults;
	}

	public long getChangedResults() {
		return changedResults;
	}

	public void setChangedResults(long changedResults) {
		this.changedResults = changedResults;
	}

	public boolean isPageNotChanged() {
		return pageNotChanged;
	}

	public void setPageNotChanged(boolean pageNotChanged) {
		this.pageNotChanged = pageNotChanged;
	}

}