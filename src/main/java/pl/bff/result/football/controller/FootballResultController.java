package pl.bff.result.football.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.bff.result.football.FootballResultService;

@RestController
@RequestMapping("/football/")
public class FootballResultController {

	private final static String LEAGUE_CODE_PATH = "leagueCode";
	private final static String DAYS_PATH = "days";
	private static final int DEFAULT_DAYS = 7;
	private static final int MAX_DAYS = 30;
	
	private FootballResultService resultService;
	
	@Autowired
	public FootballResultController(FootballResultService service) {
		this.resultService = service;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/past/{"+DAYS_PATH+"}/days")
	public List<FootballResultDTO> last(@PathVariable(LEAGUE_CODE_PATH) String leagueCode, @PathVariable(DAYS_PATH) Integer days) {
		List<FootballResultDTO> results = resultService.getAllLastByLeague(leagueCode, getDays(days));
		return results;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/next/{"+DAYS_PATH+"}/days")
	public List<FootballResultDTO> next(@PathVariable(LEAGUE_CODE_PATH) String leagueCode, @PathVariable(DAYS_PATH) Integer days) {
		List<FootballResultDTO> results = resultService.getAllNextByLeague(leagueCode, getDays(days));
		return results;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/live")
	public List<FootballResultDTO> live(@PathVariable(LEAGUE_CODE_PATH) String leagueCode) {
		List<FootballResultDTO> results = resultService.getAllLiveByLeague(leagueCode);
		return results;
	}
	
	
	private Integer getDays(Integer days) {
		if (days == null) {
			return DEFAULT_DAYS;
		}
		if (days > MAX_DAYS) {
			return MAX_DAYS;
		}
		return days;
	}
}
