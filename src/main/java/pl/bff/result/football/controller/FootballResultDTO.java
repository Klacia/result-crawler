package pl.bff.result.football.controller;

import java.time.ZonedDateTime;

import pl.bff.result.domain.enums.ResultStatus;
import pl.bff.result.football.domain.Score;

public class FootballResultDTO {

	public String homeTeam;
	public String visitorTeam;
	public Score score;
	public ResultStatus status;
	public ZonedDateTime date;
	public String league;
	public Integer round;

	@Override
	public String toString() {
		return date + " | " + status + " | " + homeTeam + " " + score.getHome() + ":" + score.getVisitor() + " " + visitorTeam + " | " + league + " | " + round;
	}
}