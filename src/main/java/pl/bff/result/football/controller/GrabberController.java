package pl.bff.result.football.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.bff.result.domain.enums.SiteType;
import pl.bff.result.site.SiteService;

@RestController
@RequestMapping("/football/grabber")
public class GrabberController {

	private final static String LEAGUE_CODE_PATH = "leagueCode";

	private SiteService siteService;

	@Autowired
	public GrabberController(SiteService siteService) {
		super();
		this.siteService = siteService;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/past")
	public String last(@PathVariable(LEAGUE_CODE_PATH) String leagueCode) {
		return grabResults(leagueCode, SiteType.PAST);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/next")
	public String next(@PathVariable(LEAGUE_CODE_PATH) String leagueCode) {
		return grabResults(leagueCode, SiteType.FIXTURES);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{"+LEAGUE_CODE_PATH+"}/live")
	public String live(@PathVariable(LEAGUE_CODE_PATH) String leagueCode) {
		return grabResults(leagueCode, SiteType.LIVE);
	}

	public String grabResults(String leagueCode, SiteType siteType) {
		return ""+siteService.grabResults(leagueCode, siteType).getNewResults();
	}

}
