package pl.bff.result.football.domain;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FootballResultRepository extends PagingAndSortingRepository<FootballResult, Long>, JpaSpecificationExecutor<FootballResult> {

	public FootballResult findByLeagueAndHomeTeamAndVisitorTeam(String league, String homeTeam, String visitorTeam);
}
