package pl.bff.result.football.domain;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type
import org.springframework.format.annotation.DateTimeFormat;

import groovy.transform.ToString;
import pl.bff.result.domain.AuditBase;
import pl.bff.result.domain.enums.ResultStatus;;

@Entity
@ToString
public class FootballResult extends AuditBase {

	private static final long serialVersionUID = 8702438768410804718L;

	@NotNull
	@Embedded
	public Score score;

	@NotNull
	@Column(nullable = false)
	public String homeTeam;
	
	@NotNull
	@Column(nullable = false)
	public String visitorTeam;
	
	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	public ResultStatus status;
	
//	@NotNull
//	@Column(nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.threeten.PersistentZonedDateTime")
//	@DateTimeFormat (pattern="dd-MM-YYYY")
    private ZonedDateTime date;
	
	@NotNull
	@Column(nullable = false)
	public String league;
	
	@NotNull
	@Column(nullable = false)
	public Integer round;

	public Score getScore() {
		return score;
	}

	public void setScore(Score score) {
		this.score = score;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getVisitorTeam() {
		return visitorTeam;
	}

	public void setVisitorTeam(String visitorTeam) {
		this.visitorTeam = visitorTeam;
	}

	public ResultStatus getStatus() {
		return status;
	}

	public void setStatus(ResultStatus status) {
		this.status = status;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Integer getRound() {
		return round;
	}

	public void setRound(Integer round) {
		this.round = round;
	}

}
