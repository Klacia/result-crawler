package pl.bff.result.football.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

import groovy.transform.ToString;

@ToString
@Embeddable
public class Score {

	@Min(0)
	@Column(name = "home_score", nullable = false)
	public Integer home = 0;

	@Min(0)
	@Column(name = "visitor_score", nullable = false)
	public Integer visitor = 0;

	public Score() {
	}
	
	public Score(String score) {
		parseScore(score);
	}

	public Score(int home, int visitor) {
		this.home = home;
		this.visitor = visitor;
	}

	public String X12Result() {
		if (home == visitor) {
			return "X";
		} else if (home > visitor) {
			return "1";
		} else if (home < visitor) {
			return "2";
		} else {
			return "NO WAY :P";
		}
	}
	
	@Override
	public String toString() {
		return home + " : " + visitor;
	}

	public Integer getHome() {
		return home;
	}

	public void setHome(Integer home) {
		this.home = home;
	}

	public Integer getVisitor() {
		return visitor;
	}

	public void setVisitor(Integer visitor) {
		this.visitor = visitor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((home == null) ? 0 : home.hashCode());
		result = prime * result + ((visitor == null) ? 0 : visitor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Score other = (Score) obj;
		if (home == null) {
			if (other.home != null)
				return false;
		} else if (!home.equals(other.home))
			return false;
		if (visitor == null) {
			if (other.visitor != null)
				return false;
		} else if (!visitor.equals(other.visitor))
			return false;
		return true;
	}
	
	private void parseScore(String score){
		String[] scoreArray = score.split(":");
		if ( scoreArray.length == 2) {
			home = Integer.valueOf(scoreArray[0]);
			visitor = Integer.valueOf(scoreArray[1]);
		}
	}
	
}
