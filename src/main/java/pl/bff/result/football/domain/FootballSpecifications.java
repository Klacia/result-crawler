package pl.bff.result.football.domain;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class FootballSpecifications {

	public static Specification<FootballResult> pastByLeague(final String league, int days) {
		return new Specification<FootballResult>(){

			final ZonedDateTime today = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS);
			
			@Override
			public Predicate toPredicate(Root<FootballResult> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.and(cb.equal(root.get(FootballResult_.league), league), 
							  cb.greaterThanOrEqualTo(root.<ZonedDateTime>get("date"), today.minusDays(days)), 
							  cb.lessThanOrEqualTo(root.<ZonedDateTime>get("date"), today));
			}
			
		};
	}
	
	public static Specification<FootballResult> nextByLeague(final String league, int days) {
		return new Specification<FootballResult>(){

			final ZonedDateTime today = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS);
			
			@Override
			public Predicate toPredicate(Root<FootballResult> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.and(cb.equal(root.get("league"), league), 
							  cb.greaterThanOrEqualTo(root.<ZonedDateTime>get("date"), today.plusDays(1)), 
							  cb.lessThanOrEqualTo(root.<ZonedDateTime>get("date"), today.plusDays(days+1)));
			}
			
		};
	}
	
	public static Specification<FootballResult> liveByLeague(final String league) {
		return new Specification<FootballResult>(){

			final ZonedDateTime today = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS);
			
			@Override
			public Predicate toPredicate(Root<FootballResult> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.and(cb.equal(root.get("league"), league), 
							  cb.greaterThan(root.<ZonedDateTime>get("date"), today), 
							  cb.lessThan(root.<ZonedDateTime>get("date"), today.plusDays(1)));
			}
			
		};
	}

}
