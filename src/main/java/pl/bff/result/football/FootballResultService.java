package pl.bff.result.football;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bff.result.domain.enums.ResultStatus;
import pl.bff.result.football.controller.FootballResultDTO;
import pl.bff.result.football.domain.FootballResult;
import pl.bff.result.football.domain.FootballResultRepository;
import pl.bff.result.football.domain.FootballSpecifications;
import pl.bff.result.football.domain.Score;

@Component
public class FootballResultService {

	private FootballResultRepository footballResultRepo;

	@Autowired
	public FootballResultService(FootballResultRepository footballResultRepo) {
		super();
		this.footballResultRepo = footballResultRepo;
	}

	public List<FootballResultDTO> getAllNextByLeague(String leagueCode, Integer days) {
		List<FootballResult> resultsList = footballResultRepo
				.findAll(FootballSpecifications.nextByLeague(leagueCode, days));
		return resultsList.stream().map(footballResultToDTOFnc).collect(Collectors.<FootballResultDTO> toList());
	}

	public List<FootballResultDTO> getAllLastByLeague(String leagueCode, Integer days) {
		List<FootballResult> resultsList = footballResultRepo
				.findAll(FootballSpecifications.pastByLeague(leagueCode, days));
		return resultsList.stream().map(footballResultToDTOFnc).collect(Collectors.<FootballResultDTO> toList());
	}

	public List<FootballResultDTO> getAllLiveByLeague(String leagueCode) {
		List<FootballResult> resultsList = footballResultRepo.findAll(FootballSpecifications.liveByLeague(leagueCode));
		return resultsList.stream().map(footballResultToDTOFnc).collect(Collectors.<FootballResultDTO> toList());
	}

	final Function<FootballResult, FootballResultDTO> footballResultToDTOFnc = new Function<FootballResult, FootballResultDTO>() {

		public FootballResultDTO apply(FootballResult footballResult) {
			FootballResultDTO dto = new FootballResultDTO();
			dto.score = footballResult.getScore();
			dto.homeTeam = footballResult.getHomeTeam();
			dto.visitorTeam = footballResult.getVisitorTeam();
			dto.league = footballResult.getLeague();
			dto.round = footballResult.getRound();
			dto.status = footballResult.getStatus();
			dto.date = footballResult.getDate();
			return dto;
		}
	};

	final Function<FootballResultDTO, FootballResult> footballResultDTOToEntityFnc = new Function<FootballResultDTO, FootballResult>() {

		@Override
		public FootballResult apply(FootballResultDTO dto) {
			FootballResult fr = new FootballResult();
			fr.setScore(dto.score);
			fr.setHomeTeam(dto.homeTeam);
			fr.setVisitorTeam(dto.visitorTeam);
			fr.setLeague(dto.league);
			fr.setRound(dto.round);
			fr.setStatus(dto.status);
			fr.setDate(dto.date);
			return fr;
		}
	};

	public SiteParsingResult<FootballResultDTO> saveParsedResults(SiteParsingResult<FootballResultDTO> resultsPast) {

		long newResults = 0;
		long changedResults = 0;

		for (FootballResultDTO parsedResult : resultsPast.getParsingResults()) {
			FootballResult oldResult = footballResultRepo.findByLeagueAndHomeTeamAndVisitorTeam(parsedResult.league,
					parsedResult.homeTeam, parsedResult.visitorTeam);
			FootballResult newResult = footballResultDTOToEntityFnc.apply(parsedResult);

			if (oldResult == null) {
				footballResultRepo.save(newResult);
				newResults++;
			} else if (isResultChanged(oldResult, newResult)) {
				oldResult = updateChanges(oldResult, newResult);
				footballResultRepo.save(oldResult);
				changedResults++;
			}
		}
		
		resultsPast.setChangedResults(changedResults);
		resultsPast.setNewResults(newResults);
		
		return resultsPast;
	}

	private FootballResult updateChanges(FootballResult oldResult, FootballResult newResult) {
		oldResult.status = newResult.getStatus();
		oldResult.score = newResult.getScore();

		ZonedDateTime oldResultDate = oldResult.getDate().withYear(newResult.getDate().getYear());
		oldResultDate = oldResult.getDate().withMonth(newResult.getDate().getMonthValue());
		oldResultDate = oldResult.getDate().withDayOfMonth(newResult.getDate().getDayOfMonth());
		oldResult.setDate(oldResultDate);
		return oldResult;
	}

	private boolean isResultChanged(FootballResult oldResult, FootballResult newResult) {
		return isDateChanged(oldResult.getDate(), newResult.getDate())
				|| isScoreChanged(oldResult.score, newResult.score)
				|| isStatusChanged(oldResult.status, newResult.status);
	}

	private boolean isDateChanged(ZonedDateTime oldDateTime, ZonedDateTime newDateTime) {
		return oldDateTime.truncatedTo(ChronoUnit.DAYS).compareTo(newDateTime.truncatedTo(ChronoUnit.DAYS)) != 0;
	}

	private boolean isScoreChanged(Score oldScore, Score newScore) {
		return !oldScore.equals(newScore);
	}

	private boolean isStatusChanged(ResultStatus oldStatus, ResultStatus newStatus) {
		return !oldStatus.equals(newStatus);
	}

}
