package pl.bff.result.football.provider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import pl.bff.result.domain.Script;
import pl.bff.result.football.controller.FootballResultDTO;
import pl.bff.result.script.GroovyScriptRunner;
import pl.bff.result.script.ScriptResult;
import pl.bff.result.script.ScriptRunner;

@Component
public class FootballGroovyScriptRunner {

	private ScriptRunner<List<FootballResultDTO>> grabber = new GroovyScriptRunner<List<FootballResultDTO>>();
	private ScriptRunner<String> hasher = new GroovyScriptRunner<String>();
	
	public ScriptResult<List<FootballResultDTO>> getResults(Script script, String pageContent){
		Map<String, Object> scriptParams = new HashMap<String, Object>();
		scriptParams.put("pageContent", pageContent);
		return grabber.runScript(script.getContent(), scriptParams);
	}
	
	public ScriptResult<String> getSiteHash(Script script, String pageContent){
		Map<String, Object> scriptParams = new HashMap<String, Object>();
		scriptParams.put("pageContent", pageContent);
		return hasher.runScript(script.getContent(), scriptParams);
	}
	
}
